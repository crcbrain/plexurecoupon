**plexurecoupon**

**Please note the following:**

- AsyncProcessingTests.cs 
Async programming

- CouponManagerTests.cs
Unit tests for coupon manager

- QuestionableCode.txt
Problems identified with the code

- database test question
Currently work in progress due to short notice and other work commitments - should be available over the weekend

periodic archiving of "expired" operational data i.e. redemptions older than 12 months (another db)
sql paging 
optimal indexing (locks) (fillfactor = 1 )  page compression
sequential insert 
dont use guid as PK 
(cluster index) 

using bigint for id fields (billions of rows)

size correctly bigint vs int
datetime2/7 

data constraints

datetimeutc ()

sysutcdatetime ()

index build
clustered index (put primary key on it as lookup) > allowed only 1 per table 
ok to index on datetime field



Provide an active list of coupons. 

index coupon table on index on id + isactive (desc) column (with preference for value 1 first)??

Determine if a consumer can redeem a coupons.

index also on customer active flag inc id + isactive (desc) 


coupon active
user active
redemption already exists?  

SELECT 1
FROM User
INNER JOIN Coupon
INNER JOIN Redemption
WHERE User is active
AND Coupon is active
AND UserID = @UserID
AND Coupon = @Coupon

Store redemptions as they occur.

no/very few db updates required (often) on the redemption table

Provide reporting on the redemptions for a specific offer.

redemption index (UserId, coupon id and redemption datetime (asc))

use stored proc


