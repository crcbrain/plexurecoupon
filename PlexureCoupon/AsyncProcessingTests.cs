using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace PlexureCoupon
{
    public class AsyncProcessingTests
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url">https://nexo.io/assets/downloads/Nexo-Whitepaper.pdf</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<long> GetDownloadResourceLength(string url, CancellationToken cancellationToken)
        {
            Task<long> task = null;
            long contentLength = 0;

            task = Task.Run(async () =>
            {

                if (cancellationToken.IsCancellationRequested)
                    throw new TaskCanceledException(task);

                using (var client = new HttpClient())
                {
                    using (var result = await client.GetAsync(url, cancellationToken))
                    {
                        if (result.IsSuccessStatusCode)
                        {
                            var contentLengthHeader = result.Content.Headers.First(h => h.Key.Equals("Content-Length")).Value ?? result.Content.Headers.First(h => h.Key.Equals("ContentLength")).Value;
                            contentLength = long.Parse(contentLengthHeader.First());
                        }
                    }
                }

                return contentLength;
            });

            return task;
        }

        public async Task<long> GetDownloadResourceLengths(string[] urls, CancellationToken cancellationToken)
        {
            List<Task<long>> taskList = new List<Task<long>>();

            var sync = new object();

            Parallel.ForEach(urls.ToList(), x =>
            {
                lock (sync)
                {
                    taskList.Add(GetDownloadResourceLength(x, cancellationToken));
                }
            });

            var task = await Task.WhenAll<long>(taskList);

            return task.Sum();
        }

        [Fact]
        public void AsyncTest_Fail()
        {
            // make this shorter to demonstrate early cancellation
            var timeoutInMs = 2;

            Assert.Throws<AggregateException>(() => AsyncTestBody(timeoutInMs));
        }

        [Fact]
        public void AsyncTest_Success()
        {
            var timeoutInMs = 20000;

            Assert.IsType<long>(AsyncTestBody(timeoutInMs));
        }

        public long AsyncTestBody(int timeoutInMs)
        {
            try
            {

                var cancellationSource = new CancellationTokenSource(timeoutInMs);

                var task = GetDownloadResourceLengths(
                    new[] {
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png",
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png",
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png",
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png",
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png",
                "https://ang.wikipedia.org/wiki/Ymele:Periodic_table_large.png"
                    }, cancellationSource.Token);

                return task.Result;


            }
            catch (AggregateException ex)
            {
                throw ex;//TODO (aggregating TaskCanceledException exceptions)
            }
            catch (TaskCanceledException ex)
            {
                throw ex;//TODO
            }
            catch (Exception ex)
            {
                throw ex;//TODO
            }
        }
    }
}
