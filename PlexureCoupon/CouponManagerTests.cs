﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PlexureCoupon
{

    #region
    public class CouponManagerTests
    {
        [Fact]
        public void CouponManager_IOCTest_NoLogger_Fail()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();

            CouponManager couponManager = null;

            var ex = Assert.Throws<ArgumentNullException>(() => couponManager = new CouponManager(null, couponProvider.Object));

            Assert.Equal(((System.ArgumentException)ex).ParamName, nameof(logger));
        }

        [Fact]
        public void CouponManager_OCTest_NoCouponProvider_Fail()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();

            CouponManager couponManager = null;

            var ex = Assert.Throws<ArgumentNullException>(() => couponManager = new CouponManager(logger.Object, null));

            Assert.Equal(((System.ArgumentException)ex).ParamName, nameof(couponProvider));
        }

        [Fact]
        public CouponManager CouponManager_IOCTest_Success()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();

            var couponManager = new CouponManager(logger.Object, couponProvider.Object);

            return couponManager;
        }

        [Fact]
        public void CouponManager_CanRedeemCouponTest_NoEvaluatorsFail_Fail()
        {
            var couponManager = CouponManager_IOCTest_Success();
            var evaluators = new object();//new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();

            var ex = Assert.ThrowsAsync<ArgumentNullException>(() => couponManager.CanRedeemCoupon(Guid.NewGuid(), Guid.NewGuid(), null));

            Assert.Equal(((System.ArgumentException)ex.Result).ParamName, nameof(evaluators));
        }

        [Fact]
        public void CouponManager_CanRedeemCouponTest_NullCoupon_Fail()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();
            var couponGuid = Guid.NewGuid();
            couponProvider.Setup(m => m.Retrieve(couponGuid)).Returns(Task.FromResult<Coupon>(null));

            var couponManager = new CouponManager(logger.Object, couponProvider.Object);
            var evaluators = new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();

            var ex = Assert.ThrowsAsync<KeyNotFoundException>(() => couponManager.CanRedeemCoupon(couponGuid, It.IsAny<Guid>(), evaluators.Object));
        }

        [Fact]
        public void CouponManager_CanRedeemCouponTest_NoEvaluator_Success()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();
            var couponGuid = Guid.NewGuid();
            var coupon = new Coupon()
            {
                Id = couponGuid
            };

            var evaluators = new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();

            evaluators.Setup(m => m.GetEnumerator()).Returns(()=>new List<Func<Coupon, Guid, bool>>().GetEnumerator());

            couponProvider.Setup(m => m.Retrieve(couponGuid)).Returns(Task.FromResult<Coupon>(coupon));

            var couponManager = new CouponManager(logger.Object, couponProvider.Object);
            
            Assert.True( couponManager.CanRedeemCoupon(couponGuid, Guid.NewGuid(), evaluators.Object).Result);
        }


        [Fact]
        public void CouponManager_CanRedeemCouponTest_EvaluatorSingleTrue_Success()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();
            var couponGuid = Guid.NewGuid();
            var coupon = new Coupon()
            {
                Id = couponGuid
            };

            var evaluators = new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();

            var evaluatorList = new List<Func<Coupon, Guid, bool>>();

            evaluatorList.Add((cp, id) => { return true; });

            evaluators.Setup(m=>m.GetEnumerator()).Returns (()=>evaluatorList.GetEnumerator());

            couponProvider.Setup(m => m.Retrieve(couponGuid)).Returns(Task.FromResult<Coupon>(coupon));

            var couponManager = new CouponManager(logger.Object, couponProvider.Object);

            Assert.True(couponManager.CanRedeemCoupon(couponGuid, Guid.NewGuid(), evaluators.Object).Result);
        }


        [Fact]
        public void CouponManager_CanRedeemCouponTest_EvaluatorSingleFail_Fail()
        {
            var logger = new Mock<ILogger>();
            var couponProvider = new Mock<ICouponProvider>();
            var couponGuid = Guid.NewGuid();
            var coupon = new Coupon()
            {
                Id = couponGuid
            };

            var evaluators = new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();

            var evaluatorList = new List<Func<Coupon, Guid, bool>>();

            evaluatorList.Add((cp, id) => { return false; });

            evaluators.Setup(m => m.GetEnumerator()).Returns(() => evaluatorList.GetEnumerator());

            couponProvider.Setup(m => m.Retrieve(couponGuid)).Returns(Task.FromResult<Coupon>(coupon));

            var couponManager = new CouponManager(logger.Object, couponProvider.Object);

            Assert.False(couponManager.CanRedeemCoupon(couponGuid, Guid.NewGuid(), evaluators.Object).Result);
        }
    }
    #endregion

    #region Classes and Interfaces
    public class CouponManager
    {
        private readonly ILogger _logger;
        private readonly ICouponProvider _couponProvider;

        public CouponManager(ILogger logger, ICouponProvider couponProvider)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _couponProvider = couponProvider ?? throw new ArgumentNullException(nameof(couponProvider));
        }

        public async Task<bool> CanRedeemCoupon(Guid couponId, Guid userId, IEnumerable<Func<Coupon, Guid, bool>> evaluators)
        {
            if (evaluators == null)
                throw new ArgumentNullException(nameof(evaluators));

            var coupon = await _couponProvider.Retrieve(couponId);

            if (coupon == null)
                throw new KeyNotFoundException();

            if (!evaluators.Any())
                return true;

            bool result = false;
            foreach (var evaluator in evaluators)
                result |= evaluator(coupon, userId);

            return result;
        }
    }

    public interface ICouponProvider
    {
        Task<Coupon> Retrieve(Guid couponId);
    }

    public class Coupon
    {
        public Guid Id { get; set; }
    }

    public interface ILogger
    {
        void Log(string payload);
    }
}
#endregion Classes and Interfaces