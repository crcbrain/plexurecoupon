﻿CREATE TABLE [dbo].[CouponRedemptionSummary] (
    [Id]              BIGINT IDENTITY (1, 1) NOT NULL,
    [UserId]          INT    NOT NULL,
    [CouponId]        INT    NOT NULL,
    [RedemptionCount] INT    CONSTRAINT [DF__CouponRed__Redem__60A75C0F] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CouponRedemptionSummary] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CouponRedemptionSummary_Coupon] FOREIGN KEY ([CouponId]) REFERENCES [dbo].[Coupon] ([Id]),
    CONSTRAINT [FK_CouponRedemptionSummary_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

