﻿CREATE TABLE [dbo].[CouponRedemption] (
    [Id]                 BIGINT             IDENTITY (1, 1) NOT NULL,
    [UserId]             INT                NOT NULL,
    [CouponId]           INT                NOT NULL,
    [UniqueCode]         NVARCHAR (250)     NOT NULL,
    [RedemptionDateTime] DATETIMEOFFSET (7) NOT NULL,
    CONSTRAINT [PK_CouponRedemption] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CouponRedemption_Coupon] FOREIGN KEY ([CouponId]) REFERENCES [dbo].[Coupon] ([Id]),
    CONSTRAINT [FK_CouponRedemption_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

