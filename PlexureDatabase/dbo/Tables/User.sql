﻿CREATE TABLE [dbo].[User] (
    [Id]            INT                IDENTITY (1, 1) NOT NULL,
    [Identifier]    UNIQUEIDENTIFIER   NOT NULL,
    [Email]         NVARCHAR (250)     NOT NULL,
    [FirstName]     NVARCHAR (100)     NOT NULL,
    [LastName]      NVARCHAR (100)     NOT NULL,
    [IsActive]      BIT                CONSTRAINT [DF_User_IsActive] DEFAULT ((0)) NOT NULL,
    [LastUpdated]   DATETIMEOFFSET (7) NULL,
    [UpdatedBy]     NVARCHAR (250)     NULL,
    [BirthDateTime] DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'external identifier ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'User', @level2type = N'COLUMN', @level2name = N'Identifier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'most systems deem this as necessary and not nullable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'User', @level2type = N'COLUMN', @level2name = N'Email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'utc datetime (international) for auditing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'User', @level2type = N'COLUMN', @level2name = N'LastUpdated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'for auditing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'User', @level2type = N'COLUMN', @level2name = N'UpdatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'not all users will have birthdays registered', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'User', @level2type = N'COLUMN', @level2name = N'BirthDateTime';

