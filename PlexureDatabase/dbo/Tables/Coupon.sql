﻿CREATE TABLE [dbo].[Coupon] (
    [Id]                 INT                NOT NULL,
    [Identifier]         UNIQUEIDENTIFIER   NOT NULL,
    [Title]              NVARCHAR (250)     NOT NULL,
    [StartDate]          DATETIMEOFFSET (7) NOT NULL,
    [EndDate]            DATETIMEOFFSET (7) NULL,
    [MaxCouponsAllUsers] INT                CONSTRAINT [DF_Coupon_MaxCouponsPerUser1] DEFAULT ((0)) NOT NULL,
    [MaxCouponsPerUser]  INT                CONSTRAINT [DF_Coupon_MaxCouponsPerUser] DEFAULT ((0)) NOT NULL,
    [IsActive]           BIT                CONSTRAINT [DF_Coupon_IsActive] DEFAULT ((0)) NOT NULL,
    [LastUpdated]        DATETIMEOFFSET (7) CONSTRAINT [DF_Coupon_LastUpdated] DEFAULT (sysdatetimeoffset()) NULL,
    [UpdatedBy]          NVARCHAR (250)     NULL,
    CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'external identifier ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'Identifier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'nvarchar for unicode chars (international)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'Title';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'utc datetime (international)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'StartDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'utc datetime (international) not all coupons will have end dates', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'EndDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'by default not active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'utc datetime (international) for auditing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'LastUpdated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'for auditing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Coupon', @level2type = N'COLUMN', @level2name = N'UpdatedBy';

